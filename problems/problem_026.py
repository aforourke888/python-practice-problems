# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if len(values) == 0:
        return None

    sum = 0
    for score in values:
        sum += score

    average = sum / len(values)
    grade = ""
    if average >= 90:
        grade = "A"
    if average < 90 and average >= 80:
        grade = "B"
    if average < 80 and average >= 70:
        grade = "C"
    if average < 70 and average >= 60:
        grade = "D"
    if average < 60:
        grade = "F"
    print(f"Your average score is {average} and your grade is {grade}")
    return grade


calculate_grade([100, 90, 85, 69, 50])
