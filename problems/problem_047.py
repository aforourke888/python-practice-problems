# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    has_lowercase = False
    has_uppercase = False
    has_digit = False
    has_special = False
    long_enuf = len(password) >= 6
    short_enuf = len(password) <= 12
    for char in password:
        if char.islower:
            has_lowercase = True
        if char.isupper:
            has_uppercase = True
        if char.isdigit:
            has_digit = True
        if char == "$" or char == "!" or char == "@":
            has_special = True
    if has_lowercase and \
       has_uppercase and \
       has_digit and \
       has_special and \
       long_enuf and \
       short_enuf:
        return True
    else:
        return False


password = "P@$$w0rd"
print(f"Valid Password: {check_password(password)}")
