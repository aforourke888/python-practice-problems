# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) < 2:
        return None

    sorted_val = sorted(values)
    second_largest = sorted_val[-2]

    return second_largest


list = [3, 8, 23, 5, 20, 83, 74]
result = find_second_largest(list)
print(result)
