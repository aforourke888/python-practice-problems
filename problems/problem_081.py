# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"


class Animal:
    def __init__(self, num_legs: int, primary_color: str):
        self.num_legs = num_legs
        self.primary_color = primary_color

    def describe(self):
        return f"{self.__class__.__name__} has {str(self.num_legs)} legs and" \
               f" is primarily {self.primary_color}"


class Dog(Animal):
    def __init__(self, num_legs: int, primary_color: str):
        super().__init__(num_legs, primary_color)

    def speak(self):
        return "borkBork!"


class Cat(Animal):
    def __init__(self, num_legs: int, primary_color: str):
        super().__init__(num_legs, primary_color)

    def speak(self):
        return "meowmeowmeowmeow\nmeowmeowmeowmeow\n" \
               "meowmeowmeowmeowmeowmeowmeowmeow"


class Snake(Animal):
    def __init__(self, num_legs: int, primary_color: str):
        super().__init__(num_legs, primary_color)

    def speak(self):
        return "SSSsssSSSss, I'm a sneaky little snaaake"


snake = Snake(0, "Green")
print(snake.describe())
print(snake.speak())

dog = Dog(3, "Brown")
print(dog.describe())
print(dog.speak())

cat = Cat(4, "Orange")
print(cat.describe())
print(cat.speak())
