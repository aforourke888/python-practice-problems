# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

import math


def halve_the_list(single_list):
    mid = math.ceil(len(single_list) / 2)
    half1 = single_list[:mid]
    half2 = single_list[mid:]
    return half1, half2


single_list = [1, 2, 3]
print(halve_the_list(single_list))
